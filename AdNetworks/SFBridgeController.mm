//
//  SFBridgeController.m
//
//  Created by Six Foot Three Foot on 27/11/13.
//
//

#import "SFBridgeController.h"
#import <Foundation/Foundation.h>
#import "BannerViewController.h"

@interface SFBridgeControllerOBJC : NSObject

@end

@implementation SFBridgeControllerOBJC

@end


void SFBridgeController::postNotification(const char *message)
{
    [[NSNotificationCenter defaultCenter] postNotificationName:[NSString stringWithCString:message encoding:NSUTF8StringEncoding] object:nil];
}

void SFBridgeController::enableAds()
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SFBANNERADSENABLE object:nil];
}

void SFBridgeController::disableAds()
{
    [[NSNotificationCenter defaultCenter] postNotificationName:SFBANNERADSDISABLE object:nil];
}
