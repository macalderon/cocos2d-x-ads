//
//  SFBridgeController.h
//
//  Created by Six Foot Three Foot on 27/11/13.
//
//

#ifndef mojowordpuzzlegames_SFBridgeController_mm
#define mojowordpuzzlegames_SFBridgeController_mm
#include "cocos2d.h"

class SFBridgeController
{
public:
    SFBridgeController();
    ~SFBridgeController();
    
    static void postNotification(const char *message);
    static void enableAds();
    static void disableAds();
};

#endif
